

														=======================
														   CRM ChatBOT -- AVA
														=======================

														
								/**
								 * @license Copyright (c) 2018, ERA-INFOTECH LIMITED. All rights reserved.
								 */											



AVA is a specialized Chatbot for providing Personalized Customer Services through Smart Apps, Web and  Social Medias (E.g.Facebook). People(Customers) will feel that 
‘Someone’ is always assigned at their services Which is not possible by engaging human support officers for a Bank.
With the help of AI based Chat BOT Banks can easily provide the difficult jobs of support without any queue and customer will think that one person is engaged from bank for only his/her support for any time which 
truly is not possible by traditional human in a call center.  								 


# Functionalities:
===================

1. Realtime Chatting for concurrent distinct users 
2. Get response from the bot across different servers.
3. Support for Chat Transcripts ...
4. Admin Panel with statistics support added..
5. Training of the bot through admin panel!!
6. Login by entering valid Mobile Number and Name
7. Conversation Language options should be English and Bangla
8. Conversation Refreshing option
9. Admin panel for viewing unanswered questions and train for static un-chained questions responses 
10. BOT will try to keep users in a perfect track with a guided conversation tour but BOT will be capable to answer any open question within the scope.
11.	Response generation from linking with databases for some defined questions
12.	Inactive session waiting timer and alert and finally auto stop option after a defined inactive period
13.	Conversation Archiving and monitoring


# Features:
=============

1.	Login for Every User
2.	Switch to Admin Panel Any Time
3.	Timer Feature & Auto Logout
4.	Language Switching Feature
5.	Intelligence of Predicting Actual User Query
6.	Chat History Store
7.	Error Log
8.	Authenticated Admin Login
9.	Statistical Data
10.	Interactive Instruction for Admin
11.	Training Maya ChatBot from Admin Panel




# Installation:
==================

Dev Server:

* `source virtual/bin/activate`
* `pip install -r requirements.txt --user`
* `python server.py`

Uwsgi Server:

* uwsgi --master --https :5003,foobar.crt,foobar.key --http-websockets  --gevent 1000 --wsgi-file server.py --callable app



It is highly encouraged to run the application in virtual environment. 

Regards, 
=========

Developer : Salman Rakin
Project Manager: Anwar Hossain

Artificial Intelligence Team
ERA-INFOTECH LIMITED.
	